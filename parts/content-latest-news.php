<article class="latest-news">
    <a href="<?php the_permalink(); ?>">
        <?php if( has_post_thumbnail()):?>
        <?php the_post_thumbnail('large'); ?>
        <?php else: ?>
        <img src="<?php echo get_template_directory_uri() . "/images/default.jpg"?>" alt="">
        <?php endif; ?>
    </a>
    
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <div class="meta-info">
        <p><?php esc_html_e('by', 'wp-devs');?> <span><?php the_author_posts_link(); ?></span>
        <?php if( has_category()):?>
        <?php esc_html_e('Categories', 'wp-devs');?>: <span><?php the_category(' '); ?></span>
        <?php endif; ?>
        <?php if( has_tag()): ?>
        <?php esc_html_e('Tags', 'wp-devs'); ?>: <span><?php the_tags('',', '); ?></span>
        <?php endif; ?>
        </p>
        <p>
            <span><?php echo esc_html(get_the_date()); ?></span>
        </p>
    </div>
    <?php the_excerpt(); ?>
</article>